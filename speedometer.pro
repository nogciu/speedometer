TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG += qt

SOURCES += main.cpp


LIBS += -lboost_system\
        -lboost_thread\
        -lboost_timer\
        -lboost_filesystem\
        -lwiringPi\
        -lrt\
        -lcrypt\
        `pkg-config opencv --libs`\

#include <iostream>
#include <wiringPi.h>
#include <boost/thread.hpp>

using namespace std;

#define ENKODER_PIN_A 12
#define ENKODER_PIN_B 13
#define LOOP_TIME 100 //ms

long long encoder1 = 0;
long long encoder1Last = 0;

void startEncoder()
{
    short n1 = LOW;
    short n1Last = LOW;

    while(true)
    {
//	std::cout << encoder1 << std::endl;
        n1 = digitalRead(ENKODER_PIN_A);

        if(n1Last == LOW && n1 == HIGH)
        {
            if(digitalRead(ENKODER_PIN_B) == LOW)
                encoder1--;
            else
                encoder1++;

        }
        n1Last = n1;
    }
}




int main(int argc, char *argv[])
{
    wiringPiSetup();

    pinMode(ENKODER_PIN_A, INPUT);
    pinMode(ENKODER_PIN_B, INPUT);

    double angleSpeed = 0.0;

    boost::thread encoderThread(&startEncoder);

    while(true)
    {
        long long diffrence = encoder1 - encoder1Last;
        double diff = (double)diffrence;
        encoder1Last = encoder1;
        //std::cout << diff << std::endl;
        angleSpeed = ((double)diffrence / 16.0) / (((double)LOOP_TIME / 1000.0));
        std::cout << angleSpeed << std::endl;

        boost::this_thread::sleep(boost::posix_time::milliseconds(LOOP_TIME));

    }




    return 0;
}
